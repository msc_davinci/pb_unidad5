import java.util.Scanner;

public class Ventas {

    public static void calcularVentas()
    {
        Scanner input = new Scanner( System.in );
        double ventas[][] = new double[ 5 ][ 4 ];

        //System.out.print( "Ingresa el número de vendedores (presiona -1 para finalizar): " );
        int vendedores = 1;//input.nextInt();

        while ( vendedores <= 4 )
        {
            System.out.print( "Ingresa los datos del vendedor " + vendedores + "\n");
            System.out.print( "Ingresa el número del producto: " );
            int producto = input.nextInt();
            System.out.print( "Ingrese el monto de la venta: " );
            double monto = input.nextDouble();

            if (producto >= 1 && producto < 6 && monto >= 0 )
                ventas[ producto - 1 ][ vendedores - 1 ] += monto;
            else
                System.out.println( "Entrada Invalida!" );

            //System.out.print( "Ingresa el número del vendedore, recuerda que es entre 1 y 4 (presiona -1 para finalizar): ");
            vendedores ++;//= input.nextInt();
        } // end while

        // total for each salesvendedores
        double ventasVendedoresTotal[] = new double[ 4 ];

        // display the table
        for ( int column = 0; column < 4; column++ )
            ventasVendedoresTotal[ column ] = 0;

        System.out.printf( "%8s%14s%14s%14s%14s%10s\n",
                "Producto", "Vendedor 1", "Vendedor 2",
                "Vendedor 3", "Vendedor 4", "Total" );

        for ( int row = 0; row < 5; row++ )
        {
            double productTotal = 0.0;
            System.out.printf( "%8d", ( row + 1 ) );

            for ( int column = 0; column < 4; column++ ) {
                System.out.printf( "%14.2f", ventas[ row ][ column ] );
                productTotal += ventas[ row ][ column ];
                ventasVendedoresTotal[ column ] += ventas[ row ][ column ];
            } // end for

            System.out.printf( "%10.2f\n", productTotal );
        } // end for

        System.out.printf( "%8s", "Total" );

        for ( int column = 0; column < 4; column++ )
            System.out.printf( "%14.2f", ventasVendedoresTotal[ column ] );

        System.out.println();
    }

    public static void main(String[] args) {
        calcularVentas();
    }
}
